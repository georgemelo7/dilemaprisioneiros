package algoritmogenetico;

public abstract class Individuo implements Comparable<Individuo>{

	private double aptidao;
        private int contador;
        
        public Individuo(){
            this.aptidao=0;
            this.contador=0;
        }

	public double getAptidao() {
            return this.aptidao;
	}
        
        public void setAptidao(double valor){
            this.aptidao=valor;
        }
        
        public int getContador(){
            return this.contador;
        }
        
        public void setContador(){
            this.contador++;
        }
        
        public abstract double getGene(int posicao);
        
        public abstract void fitnessPessoal(Individuo solucao,int opcao, int cadeia, boolean bonus);
        
        public abstract void fitnessColetivo(Individuo solucao, int opcao, int cadeia, boolean bonus);
        
        public abstract void calcularFitness(Individuo[] solucao, int opcao, int cadeia, boolean bonus);
    
	public abstract void sofrerMutacao(double taxaMutacao, double desvio, int gAtual, int maxGeracao);
        
        public abstract double[] getGenes();
        
        public abstract int[] tipoGene(); 
        

}
