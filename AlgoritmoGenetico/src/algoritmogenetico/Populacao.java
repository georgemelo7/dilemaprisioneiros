package algoritmogenetico;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import utilitarios.MersenneTwisterFast;

public  class Populacao{
        
        private final List<Individuo> individuos;
        static MersenneTwisterFast gerador = new MersenneTwisterFast();
        
        public Populacao(){
            this.individuos= new ArrayList<>();
        }
        
        public List<Individuo> getPopulacao(){
            return this.individuos;
        }
        
        public void ordenar(){
            Collections.sort(individuos);
        }
        
        public void calcularAptidao(int opcao, int cadeia, boolean bonus) {
            switch(opcao){
                //estrategia individual
                case 0: primeiraOpcao(cadeia, bonus);
                        break;
                //estrategia coletiva        
                case 1: segundaOpcao(cadeia, bonus);
                        break;
                //comparacao com 10% e individual        
                case 2: terceiraOpcao(0, cadeia, bonus);
                        break;
                //comparacao com 30% e individual        
                case 3: quartaOpcao(0, cadeia, bonus);
                        break;
                //comparacao com 10% e coletivo
                case 4: terceiraOpcao(1, cadeia, bonus);
                        break;
                //comparacao com 30% e coletivo         
                default: quartaOpcao(1, cadeia, bonus);
                         break;
            }
	}
        
        //IndividuoPessoal
        private void primeiraOpcao(int cadeia, boolean bonus){
            int[] usados = new int[this.individuos.size()];
            
            //faz o calculo para todos os individuos
            for(int i=1;i<=this.individuos.size();i++){
                int[] prisioneiro = {8000, 8000};
                //faz o calculo para dois individuos que estarão como prisioneiros
                for(int j=0;j<2;j++){
                    if(prisioneiro[0]==8000){
                    int indi=gerador.nextInt(this.individuos.size());
                    prisioneiro[j]=indi;
                    usados[i-1]=indi;
                    }else{
                        boolean novo = false;
                        do{
                            int posicao= gerador.nextInt(this.individuos.size());
                            for(int k=0;k<usados.length;k++){
                                if(usados[usados.length-1]!=posicao) {
                                    prisioneiro[j]=posicao;
                                    usados[i-1]=posicao;
                                    novo=true;
                                }
                            }
                        }while(!novo);
                    }
                }
                this.individuos.get(prisioneiro[0]).fitnessPessoal(this.individuos.get(prisioneiro[1]), 0, cadeia, bonus);
                this.individuos.get(prisioneiro[1]).fitnessPessoal(this.individuos.get(prisioneiro[0]), 0, cadeia, bonus);
                for(int j: prisioneiro){
                    this.individuos.get(j).setContador();
                }
            }
        }
        
        private void segundaOpcao(int cadeia, boolean bonus){
            int[] usados = new int[this.individuos.size()];
            
            for(int i=1;i<=this.individuos.size();i++){
                int[] prisioneiro = {8000, 8000};
                for(int j=0;j<2;j++){
                    if(prisioneiro[0]==8000){
                        int indi=gerador.nextInt(this.individuos.size());
                        prisioneiro[j]=indi;
                        usados[i-1]=indi;
                    } 
                    else{
                        boolean novo = false;
                        do{
                            int posicao= gerador.nextInt(this.individuos.size());
                            for(int k=0;k<usados.length;k++){
                                if(usados[usados.length-1]!=posicao) {
                                    prisioneiro[j]=posicao;
                                    usados[i-1]=posicao;
                                    novo=true;
                                }
                            }
                        }while(!novo);
                    }
                }
                this.individuos.get(prisioneiro[0]).fitnessColetivo(this.individuos.get(prisioneiro[1]), 0, cadeia, bonus);
                this.individuos.get(prisioneiro[1]).fitnessColetivo(this.individuos.get(prisioneiro[0]), 0, cadeia, bonus);
                for(int j: prisioneiro){
                    this.individuos.get(j).setContador();
                }
            }
        }
        
        private void terceiraOpcao(int opcao, int cadeia, boolean bonus){
            int tamanho=this.individuos.size()*10/100;
            for(Individuo indi : this.individuos){
                Individuo[] comparar= new Individuo[tamanho];
                for(int i=0; i<tamanho;i++){
                    comparar[i]= this.individuos.get(gerador.nextInt(this.individuos.size()));
                }
                indi.calcularFitness(comparar, opcao, cadeia, bonus);
                indi.setContador();
            }
        }
        private void terceiraOpcao(Individuo indi, int opcao, int cadeia, boolean bonus){
            int tamanho=this.individuos.size()*10/100;
            Individuo[] comparar= new Individuo[tamanho];
            for(int i=0; i<tamanho;i++){
                comparar[i]= this.individuos.get(gerador.nextInt(this.individuos.size()));
            }
            indi.calcularFitness(comparar, opcao, cadeia, bonus);
            indi.setContador();
            
        }
        
        private void quartaOpcao(int opcao, int cadeia, boolean bonus){
            int tamanho=this.individuos.size()*30/100; 
            for(Individuo indi : this.individuos){
                Individuo[] comparar= new Individuo[tamanho];
                for(int i=0; i<tamanho;i++){
                    comparar[i]= this.individuos.get(gerador.nextInt(this.individuos.size()));
                }
                indi.calcularFitness(comparar, opcao, cadeia, bonus);
                indi.setContador();
            }
        }
        
        private void quartaOpcao(Individuo indi, int opcao, int cadeia, boolean bonus){
            int tamanho=this.individuos.size()*30/100; 
            Individuo[] comparar= new Individuo[tamanho];
            for(int i=0; i<tamanho;i++){
                comparar[i]= this.individuos.get(gerador.nextInt(this.individuos.size()));
            }
            indi.calcularFitness(comparar, opcao, cadeia, bonus);
            indi.setContador();
            
        }
        
        public void gerarPopulacao(int tamanhoPopulacao, int tamanhoGenes){
            for(int i=1;i<=tamanhoPopulacao;i++){
                IndividuoReal temp= new IndividuoReal(tamanhoGenes);
                this.individuos.add(temp);
            }
        }
        
        public void resultado(){
                System.out.print(this.individuos.get(0));
                double media=0;
                for(Individuo indi: this.individuos){
                    media+=indi.getAptidao();
                }
                media=media/this.individuos.size();
                System.out.print(media+";");
        }
        
        public void tipico(){
            String fenotipo ="[";
            for(double i : this.individuos.get(this.individuos.size()/2).getGenes()){
                if(i<0.5) fenotipo+="C, ";
                else fenotipo+="D, ";
            }
            fenotipo+="]";
            System.out.println(fenotipo);
            String genotipo ="[";
            for(double i : this.individuos.get(this.individuos.size()/2).getGenes()){
                genotipo+=String.valueOf(i)+", ";
            }
            genotipo+="]";
            System.out.println(genotipo);
            
        }
        
        public void verGenotipo(){
            int[] aux= new int[2];
            /*
            String genotipo ="[";
            for(double i : this.individuos.get(0).getGenes()){
                genotipo+=String.valueOf(i)+", ";
            }
            genotipo+="]";
            aux=this.individuos.get(0).tipoGene();
            System.out.print(genotipo+" totalCop= "+aux[0]+" totalIndi= "+aux[1]);
            */
            int totalIndi=0;
            int totalCop=0;
            for(Individuo indi: this.individuos){
               aux= indi.tipoGene();
               totalCop+=aux[0];
               totalIndi+=aux[1];
            }
            double mediaCop=(double)totalCop/this.individuos.size();
            double mediaIndi=(double)totalIndi/this.individuos.size();
            System.out.print(" "+mediaIndi+"; "+mediaCop+"; ");
            
        }
        
        public void crossover(int[] pais, int numeroGenes, double taxaMutacao, double desvio, double taxaCruzamento, int caso, int cadeia, int gAtual, int maxGeracao, boolean bonus){
            double k= gerador.nextDouble(true, true);
            if(k<=taxaCruzamento){
                double[] filho1 = new double[numeroGenes];
                double[] filho2 = new double[numeroGenes];
                double alfa = gerador.nextDouble(true, true);
                for(int i=0;i<numeroGenes;i++){
                    filho1[i]= this.individuos.get(pais[0]).getGene(i)+(this.individuos.get(pais[1]).getGene(i)-this.individuos.get(pais[0]).getGene(i))*alfa;
                }
                for(int i=0;i<numeroGenes;i++){
                    filho2[i]= this.individuos.get(pais[1]).getGene(i)+(this.individuos.get(pais[0]).getGene(i)-this.individuos.get(pais[1]).getGene(i))*alfa;
                }
                Individuo indi1 = new IndividuoReal(filho1);
                Individuo indi2 = new IndividuoReal(filho2);
                indi1.sofrerMutacao(taxaMutacao, desvio, gAtual, maxGeracao);
                indi2.sofrerMutacao(taxaMutacao, desvio, gAtual, maxGeracao);
                //calcular fitness aqui
                switch(caso){
                    case 0: indi1.fitnessPessoal(indi2, 0, cadeia, bonus);
                            indi1.setContador();
                            indi2.fitnessPessoal(indi1, 0, cadeia, bonus);
                            indi2.setContador();
                            break;
                    case 1: indi1.fitnessColetivo(indi2, 0, cadeia, bonus);
                            indi1.setContador();
                            indi2.fitnessColetivo(indi1, 0, cadeia, bonus);
                            indi2.setContador();
                            break;
                    case 2: terceiraOpcao(indi1, 0, cadeia, bonus);
                            terceiraOpcao(indi2, 0, cadeia, bonus);
                            break;
                    case 3: quartaOpcao(indi1, 0, cadeia, bonus);
                            quartaOpcao(indi2, 0, cadeia, bonus);
                            break;
                    case 4: terceiraOpcao(indi1, 1, cadeia, bonus);
                            terceiraOpcao(indi2, 1, cadeia, bonus);
                            break;
                    default: quartaOpcao(indi1, 1, cadeia, bonus);
                             quartaOpcao(indi2, 1, cadeia, bonus);
                }
                
                int opcao = gerador.nextInt(3);
                switch(opcao){
                    case 0: trocarPais(indi1, indi2, pais);
                    case 1: trocarPiores(indi1, indi2);
                    case 3: trocarVelhos(indi1, indi2);
                }
            }else{
                Individuo indi1 = new IndividuoReal(this.individuos.get(pais[0]).getGenes());
                Individuo indi2 = new IndividuoReal(this.individuos.get(pais[1]).getGenes());
                indi1.sofrerMutacao(taxaMutacao, desvio, gAtual, maxGeracao);
                indi2.sofrerMutacao(taxaMutacao, desvio, gAtual, maxGeracao);
                //calcular fitness aqui
                
                switch(caso){
                    case 0: indi1.fitnessPessoal(indi2, 0, cadeia, bonus);
                            indi1.setContador();
                            indi2.fitnessPessoal(indi1, 0, cadeia, bonus);
                            indi2.setContador();
                            break;
                    case 1: indi1.fitnessColetivo(indi2, 0, cadeia, bonus);
                            indi1.setContador();
                            indi2.fitnessColetivo(indi1, 0, cadeia, bonus);
                            indi2.setContador();
                            break;
                    case 2: terceiraOpcao(indi1, 0, cadeia, bonus);
                            terceiraOpcao(indi2, 0, cadeia, bonus);
                            break;
                    case 3: quartaOpcao(indi1, 0, cadeia, bonus);
                            quartaOpcao(indi2, 0, cadeia, bonus);
                            break;
                    case 4: terceiraOpcao(indi1, 1, cadeia, bonus);
                            terceiraOpcao(indi2, 1, cadeia, bonus);
                            break;
                    default: quartaOpcao(indi1, 1, cadeia, bonus);
                             quartaOpcao(indi2, 1, cadeia, bonus);
                }
                int opcao = gerador.nextInt(3);
                switch(opcao){
                    case 0: trocarPais(indi1, indi2, pais);
                    case 1: trocarPiores(indi1, indi2);
                    case 3: trocarVelhos(indi1, indi2);
                }
            }
        }
        
        private void trocarPais(Individuo indi1, Individuo indi2, int[] vetor){
            this.individuos.set(vetor[0], indi1);
            this.individuos.set(vetor[1], indi2);
        }
        
        private void trocarPiores(Individuo indi1, Individuo indi2){
            this.individuos.set(this.individuos.size()-1, indi1);
            this.individuos.set(this.individuos.size()-2, indi2);
        }
        
        private void trocarVelhos(Individuo indi1, Individuo indi2){
            int maior=this.individuos.size()-1;
            for(int i =this.individuos.size()-1; i>0; i--){
                if(this.individuos.get(i).getContador()<this.individuos.get(i-1).getContador()) maior=i-1;
            }
            this.individuos.set(maior, indi1);
            maior=this.individuos.size()-1;
            for(int i =this.individuos.size()-1; i>0; i--){
                if(this.individuos.get(i).getContador()<this.individuos.get(i-1).getContador()) maior=i-1;
            }
            this.individuos.set(maior, indi2);
        }
        
        public void selecao(int tamanhoPopulacao, int numeroGenes, double taxaMutacao, double desvio, int tamanhoTorneio, int filhos, double taxaCruzamento, int opcao, int cadeia, int gAtual, int maxGeracao, boolean bonus){
        //gera a quantidade de torneios necessários para se gerar os filhos dados
        for(int i=1;i<=filhos/2;i++){
            int[] escolhidos=new int[2];
            double k = 0.85;//fator de torneio
            int maior=1000;
            int menor=0;
            //faz dois torneios para selecionar dois pais
            for(int j=1; j<=2;j++){
                ArrayList<Integer> torneio = new ArrayList<>();
                int contador=0;
                //gera o numero de individuos necessários para o torneio
                do{
                    int indi = gerador.nextInt(tamanhoPopulacao);
                    if(contador==0){
                            contador++;
                            torneio.add(indi);
                    }else{
                        //insere um novo individuo sem repetição
                        for(int l=0;l<contador;l++){
                            if(indi==torneio.get(l)) break;
                            if(contador-l==1){
                                contador++;
                                torneio.add(indi);
                            }
                        }
                    }
                }while(contador!=tamanhoTorneio);
                //pecorre o vetor de torneio e verifica qual tem maior fitness
                for(int l:torneio){
                    if(l<maior) maior=l;
                }
                //adiciona uma individuo aleatorio
                menor=torneio.get(gerador.nextInt(tamanhoTorneio));
                
                double r= gerador.nextDouble(true, true);
                escolhidos[j-1]=(r<=k) ? maior : menor;
            }
            crossover(escolhidos, numeroGenes, taxaMutacao, desvio, taxaCruzamento, opcao, cadeia, gAtual, maxGeracao, bonus);
        }
        //mutacao
        }

}
