package algoritmogenetico;

import java.util.ArrayList;

public class Principal {
    
    public static void main(String[] args){
        ArrayList<Populacao> populacoes = new ArrayList<>();
        int tamanhoPopulacao=40, maxGeracoes=1500, geracaoAtual=1, numeroGenes=30, tamanhoTorneio=3, filhos=6, opcao=2, tamanhoCadeia=1;
        double taxaMutacao=0.01, desvio=0.9, taxaCruzamento=0.9;
        boolean bonus=true;
        for(int i=1;i<=5;i++){
        Populacao populacao= new Populacao();
        populacao.gerarPopulacao(tamanhoPopulacao, numeroGenes);
        populacao.calcularAptidao(opcao, tamanhoCadeia, bonus);
        populacoes.add(populacao);
        }
        do{
          for(Populacao populacao : populacoes){
            populacao.ordenar();
          //populacao.calcularAptidao(opcao, tamanhoCadeia);
            populacao.resultado();
            populacao.verGenotipo();
            populacao.selecao(tamanhoPopulacao, numeroGenes, taxaMutacao, desvio, tamanhoTorneio, filhos, taxaCruzamento, opcao, tamanhoCadeia, geracaoAtual, maxGeracoes, bonus);
          }
          System.out.println();
          geracaoAtual++;
        }while(maxGeracoes>=geracaoAtual);
        populacoes.get(populacoes.size()-1).tipico();
    }
	
        
}
