package algoritmogenetico;

import utilitarios.MersenneTwisterFast;

public class IndividuoReal extends Individuo{
    
    private final double[] vetor;
    
    static MersenneTwisterFast gerador = new MersenneTwisterFast();

    public IndividuoReal(int tamanhoVetor) {
        
        super();
        
        this.vetor= new double[tamanhoVetor];
        
        for(int i=0; i<tamanhoVetor;i++){
            this.vetor[i]= geneAleatorio();
        }
        
    }
    
    public IndividuoReal(double[] vetor){
        
        super();
        
        this.vetor=vetor;
      
    }
    
    private double geneAleatorio(){
      return gerador.nextDouble(true, true);
        
    }
    
    @Override
    public double getGene(int posicao){
        return this.vetor[posicao];
    }
    
    @Override
    public double[] getGenes(){
        return this.vetor;
    }
    
    @Override
    public void sofrerMutacao(double taxaMutacao, double desvio, int gAtual, int maxGeracao) {
        for(int i=0; i<this.vetor.length;i++){
            double r = gerador.nextDouble(true, true);
            if(r<=taxaMutacao){
                boolean positivo=false;
                double desv =1-desvio*gAtual/maxGeracao;
                do{
                    double valor = this.vetor[i]+gerador.nextGaussian()*desv;
                    if(valor>=0.0 && valor<=1.0) {
                        this.vetor[i]=valor;
                        positivo=true;
                    }
                }while(!positivo);
            }
        }
        
    }
    
    private double aplicarBonus(int cadeia){
        double bonus=0;
        int contador=0;
        for(int i=0;i<this.vetor.length-1;i++){
            if(this.vetor[i]<0.5) ++contador;
            if(contador==cadeia) {
                bonus+=0.5;
                contador=0;
            }
        }
        for(int i=0;i<this.vetor.length;i++){
            if(this.vetor[i]<0.5) bonus+=0.5-this.vetor[i];
        }
        
        return bonus;
    }

    @Override
    public void fitnessPessoal(Individuo outro, int opcao, int cadeia, boolean bonus) {
        double fitness=0;
        //pecorre todos os genes
        for(int i=0;i<outro.getGenes().length;i++){
            if(this.vetor[i]>=0.5){
                if(outro.getGene(i)>=0.5){
                    fitness+=1/120;
                }else{
                    fitness+=1;
                }
            }else{
                if(outro.getGene(i)<0.5){
                    fitness+=1/6;
                }
            }
        }
        if(bonus) fitness+=aplicarBonus(cadeia);
        if(opcao==0){
            this.setAptidao(fitness);
        }else{ 
            this.setAptidao(this.getAptidao()+fitness);
        }
        
    }
    
    @Override
    public void fitnessColetivo(Individuo solucao, int opcao, int cadeia, boolean bonus){
        
        double fitness=0;
        
        for(int i=0;i<solucao.getGenes().length;i++){
            if(this.vetor[i]>=0.5){
                if(solucao.getGene(i)<0.5){
                    fitness+=1/120;
                }
            }else{
                if(solucao.getGene(i)>=0.5){
                    fitness+=1/120;
                }else{
                    fitness+=1;
                }
            }
        }
        if(bonus) fitness+=aplicarBonus(cadeia);
        
        if(opcao==0){
            this.setAptidao(fitness);
        }else{
            this.setAptidao(this.getAptidao()+fitness);
        }
    }
    //calcular o fitness comparando mais que um individuo
    @Override
    public void calcularFitness(Individuo[] solucao, int opcao, int cadeia, boolean bonus){
        for(Individuo sol: solucao){
            if(opcao==0) fitnessPessoal(sol, 1, cadeia, bonus);
            else fitnessColetivo(sol, 1, cadeia, bonus);
        }
        double fitness=this.getAptidao()/solucao.length;
        this.setAptidao(fitness);
    }
    
    @Override
    public int[] tipoGene(){
        int[] tipos= {0, 0};
        for(double gene:this.vetor){
            if(gene<0.5) tipos[0]++;
            else tipos[1]++;
        }
        return tipos;
    }
    
    @Override
    public String toString(){
        /*
        String genotipo ="[";
        for(double i : this.vetor){
            genotipo+=String.valueOf(i)+", ";
        }
        genotipo+="]";
        */
        return String.format("%.0f; ", getAptidao());
        
    } 

    @Override
    public int compareTo(Individuo o) {
        
        if (this.getAptidao() > o.getAptidao()) {
            
            return -1;
            
        }
        if (this.getAptidao() < o.getAptidao()) {
            
            return 1;
            
        }
        
        return 0;
        
        }
    

}
